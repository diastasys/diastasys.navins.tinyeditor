﻿var editor= null;
function LoadHtml(HTMLData)
{
    // TinyEditor
    if (editor == null) {
        document.getElementById("tinyeditor").value = HTMLData;
        editor = new TINY.editor.edit('editor', {
            id: 'tinyeditor',
            width: 700,
            height: 300,
            cssclass: 'tinyeditor',
            controlclass: 'tinyeditor-control',
            rowclass: 'tinyeditor-header',
            dividerclass: 'tinyeditor-divider',
            controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
                'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
                'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
                'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'copy', 'cut', 'paste'],
            footer: true,
            fonts: ['Verdana', 'Arial', 'Georgia', 'Trebuchet MS'],
            xhtml: true,
            cssfile: 'custom.css',
            bodyid: 'editor',
            footerclass: 'tinyeditor-footer',
            toggle: { text: 'source', activetext: 'wysiwyg', cssclass: 'toggle' },
            resize: { cssclass: 'resize' }
        });
    }
    else {
        editor.toggle(0);
        document.getElementById("tinyeditor").value = HTMLData;
        editor.toggle(1);
        MAINBODY.focus();
    }
}

function RequestSave() {
    editor.post();
    var html = document.getElementById("tinyeditor").value;
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('SaveCompleted', [html]);
}
