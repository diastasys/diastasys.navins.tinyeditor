﻿using Microsoft.Dynamics.Framework.UI.Extensibility;

namespace Diastasys.Navins.WYSIWYG
{
  [ControlAddInExport("Diastasys.Navins.WYSIWYG")]
  public interface iDiastasysTinyEditorControl
  {
    [ApplicationVisible]
    event ApplicationEventHandler ControlAddInReady;

    [ApplicationVisible]
    event ApplicationEventHandler HTMLLoaded;

    [ApplicationVisible]
    event ApplicationEventHandler EditorGotFocus;

    [ApplicationVisible]
    event ApplicationEventHandler EditorLostFocus;

    [ApplicationVisible]
    event ApplicationEventHandler HTMLModified;

    [ApplicationVisible]
    event SaveRequestedEventHandler SaveCompleted;

    [ApplicationVisible]
    void LoadHtml(string HTMLData);

    [ApplicationVisible]
    void RequestSave();
  }
}
